import logo from '../assets/img/clashLogo.png';
export const Navbar = () => {
    return(
        <>
        <nav className="navbar navbar-expand-lg bg-navbar navbar-light">
            <div className="container-fluid">
                <img src={logo} className="mx-auto" width="180px" alt="" />
            </div>  
        </nav>
        </>
    )
}