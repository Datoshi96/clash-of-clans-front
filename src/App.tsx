import './App.css';
import { Navbar } from './components/navbar';
import { ListClans } from './views/ListClans';


function App() {
  return (
    <div className="App bg-brown">
      <Navbar />
      <ListClans />
    </div>
  );
}

export default App;
