import { useEffect, useState } from "react"
import axios from 'axios';
interface Label {
    id: number;
    name: string;
    iconUrls: {
        small: string;
        medium: string
    },
}

interface Clan {
    tag: string;
    name: string;
    type: string;
    location: {
        id: number;
        name: string;
        isCountry: boolean;
        countryCode: string;
    };
    badgeUrls: {
        small: string;
        large: string;
        medium: string;
    };
    clanLevel: number;
    clanPoints: number;
    clanVersusPoints: number;
    requiredTrophies: number;
    warFrequency: string;
    warWinStreak: number;
    warWins: number;
    warTies: number;
    warLosses: number;
    isWarLogPublic: boolean;
    warLeague: {
      id: number;
      name: string
    };
    members: number;
    labels:Label[];
    chatLanguage: {
        id: number;
        name: string;
        languageCode: string
      };
      requiredVersusTrophies: number;
      requiredTownhallLevel: number
}

export const ListClans = () => {

    const [clans, setClans] = useState<Clan[]>();
    const [filter, setFilter] = useState<string>("");

    const getClans = (name:string) => {
        let config = {
            headers:{ 
                Accept: 'application/json',
            }
        }
        axios.get(`https://wpug4qyeco.api.quickmocker.com/getclans`,config).then( res =>{
            console.log(res.data.data);
            setClans(res.data.data);
        }).catch(error => {
            console.log(error);
        })
        

    }
    const searchText = (e:any) =>{
        setFilter(e.target.value);
    }
    useEffect(() => {
        getClans('colombia');
    },[])
    return(
        <div className="container mt-5">
            <h1 className="font-luckies">SEARCH CLAN:</h1>
            <div className="input-group mb-3">
                <input type="text" onChange={(text) => searchText(text)} className="form-control" 
                placeholder="search by name, status or location" aria-describedby="button-addon2"/>
            </div>
            <div className="table-responsive">
                <table className="table bg-yellow table-striped ">
                    <thead>
                        <tr className="font-luckies">
                            <th>Name</th>
                            <th>Status</th>
                            <th>Level</th>
                            <th>Points</th>
                            <th>Location</th>
                            <th>ChaTLenguage</th>
                            <th>Icon</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            clans &&
                            clans.filter((clan) => {
                                if (filter === ""){
                                    return clan
                                }else if(clan.name.toLowerCase().includes(filter.toLowerCase()) || clan.type.toLowerCase().includes(filter.toLowerCase()) ||clan.location.name.toLowerCase().includes(filter.toLowerCase()) ){
                                    return clan
                                }
                            }).map((clan) => {
                                return <tr key={clan.tag}>
                                            <td>{clan.name}</td>
                                            <td>{clan.type}</td>
                                            <td>{clan.clanLevel}</td>
                                            <td>{clan.clanPoints}</td>
                                            <td>{clan.location.name}</td>
                                            <td>{clan.chatLanguage.name}</td>
                                            <td><img src={clan.badgeUrls.small} alt="" width="30px" /></td>
                                        </tr>
                                }
                            )

                        }
                        
                    </tbody>
                </table>
            </div>
           
        </div>
    )
}